#!/usr/bin/env node

const http = require('http');
const port = process.env.PORT || 1337;

const server = http.createServer();

server.on('request', (request, response) => {
    const { headers, method, url } = request;
    let body = [];

    request.on('error', (err) => {
        console.error(err);
    });

    request.on('data', (chunk) => {
        body.push(chunk);
    });

    request.on('end', () => {
        body = Buffer.concat(body).toString();

        response.on('error', (err) => {
            console.error(err);
        });

        const hash = sanitizeUrl(url);

        if (!hash) {
            console.log(`No request handler found for ${url}`);
            response.writeHead(404, { 'Content-Type': 'text/plain' });
            response.write('404 Not found');
            response.end();
            return;
        }

        const rawResponse = loadRequest(hash);
        const mappedResponse = mapData(rawResponse);

        response.statusCode = 200;
        response.setHeader('Content-Type', 'application/json');

        const responseBody = {
            headers, method, url, body, rawResponse, mappedResponse
        };

        response.write(JSON.stringify(responseBody));
        response.end();
    });
});


function sanitizeUrl(key) {
    let val;
    switch (key) {
        case '/0cc175b9c0f1b6a831c399e269772661':
            val = 'A';
            break;
        case '/92eb5ffee6ae2fec3ad71c777531578f':
            val = 'B';
            break;
        case '/4a8a08f09d37b73795649038408b5f33':
            val = 'C';
            break;
        default:
            val = false;
    }
    return val;
}


function loadRequest(request) {
    console.log (`Fetching data for '${request}'`);
    let response;

    switch (request) {
        case 'A':
            response = {
                Werkgever: 'A',
                Beleggingswaarde: '715060',
                'Actief (aantal)': '182',
                'Part-time (aantal)': '38',
                'Full-time (aantal)': '144',
                'Geslacht Man (aantal)': '101',
                'Geslacht Vrouw (aantal)': '81',
                'Geslacht Onbekend (aantal)': '0',
                'Burgerlijke staat = 2': '25',
                'Burgerlijke staat = 3': '37',
                'Burgelijke staat = 5': '120'
            };
            break;
        case 'B':
            response = {
                Werkgever: 'B',
                Beleggingswaarde: '1245006',
                'Actief (aantal)': '712',
                'Part-time (aantal)': '256',
                'Full-time (aantal)': '456',
                'Geslacht Man (aantal)': '412',
                'Geslacht Vrouw (aantal)': '300',
                'Geslacht Onbekend (aantal)': '0',
                'Burgerlijke staat = 2': '260',
                'Burgerlijke staat = 3': '200',
                'Burgelijke staat = 5': '252'
            };
            break;
        case 'C':
            response = {
                Werkgever: 'C',
                Beleggingswaarde: '5240',
                'Actief (aantal)': '26',
                'Part-time (aantal)': '8',
                'Full-time (aantal)': '18',
                'Geslacht Man (aantal)': '20',
                'Geslacht Vrouw (aantal)': '6',
                'Geslacht Onbekend (aantal)': '0',
                'Burgerlijke staat = 2': '2',
                'Burgerlijke staat = 3': '10',
                'Burgelijke staat = 5': '6'
            };
            break;
        default:
            response = false;
    }

    return response;
}


function mapData(rawResponse) {
    const data = {
        data: {
            graphs: [
                {
                    type: 'participants',
                    labels: 'deelnemers',
                    data: [
                        parseInt(rawResponse['Actief (aantal)'], 10)
                    ]
                },
                {
                    type: 'sex',
                    labels: [
                        'Man', 'Vrouw'
                    ],
                    data: [
                        parseInt(rawResponse['Geslacht Man (aantal)'], 10), 
                        parseInt(rawResponse['Geslacht Vrouw (aantal)'], 10)
                    ]
                },
                {
                    type: 'employment',
                    labels: [
                        'Full-time', 'Part-time'
                    ],
                    data: [
                        parseInt(rawResponse['Full-time (aantal)'], 10), 
                        parseInt(rawResponse['Part-time (aantal)'], 10)
                    ]
                },
                {
                    type: 'investments',
                    labels: [
                        'historisch rendement', 'pessimistisch rendement', '4% bruto rendement'
                    ],
                    data: [
                        40000, 8000, 30000
                    ]
                },
                {
                    type: 'status',
                    labels: [
                        'gehuwd', 'gescheiden', 'samenwonend', 'alleenstaand'
                    ],
                    data: [
                        parseInt(rawResponse['Burgerlijke staat = 2'], 10),
                        parseInt(rawResponse['Burgerlijke staat = 3'], 10),
                        parseInt(rawResponse['Burgelijke staat = 5'], 10),
                        0
                    ]
                }
            ]
        },
        status: 0
    };
    return data;
}

server.listen(port);
console.log('Server running at http://localhost:%d', port);
